from datetime import date

from django.db.models import Q
from rest_framework.permissions import BasePermission

from restaurant.models import Restaurant


class HasValidLicense(BasePermission):
    def has_permission(self, request, view):
        return date.today() < request.user.license_expiration_date


class IsOwner(BasePermission):
    def has_permission(self, request, view):
        restaurant_id = view.kwargs.get('restaurant_id', None)
        restaurant = Restaurant.objects.get(pk=restaurant_id)
        return request.user.id == restaurant.account_id