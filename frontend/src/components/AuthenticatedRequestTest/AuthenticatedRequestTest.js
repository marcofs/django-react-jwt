import React, { useContext, useState } from 'react';

import Button from 'react-bootstrap/Button';

import { UserContext } from '../../contexts/userContext.js'
import { getCategories } from '../../api/rest.js'

function AuthenticatedRequestTest() {
  const [categoriesResult, setCategoriesResult] = useState();
  const {user, setUser, isUserLoggedIn} = useContext(UserContext)

  const requestGetCategories = (restaurantId) => {
    getCategories(restaurantId)
    .then((data)=>{
      setCategoriesResult(data.categories)
    }).catch((error)=> {
      setUser(null);
    });
  }

  return (<div className="mt-2">
      <React.Fragment>
        <Button onClick={() => requestGetCategories(1)}>GET Categories</Button>
        <ul>
          {categoriesResult?.map(c => <li>{c.name}</li>)}
        </ul>
      </React.Fragment>
    </div>
  )
}

export default AuthenticatedRequestTest;
