import json

from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from account.permissions import HasValidLicense, IsOwner
from restaurant.models import Restaurant


@api_view(['PUT'])
@permission_classes((IsAuthenticated, HasValidLicense, IsOwner))
def update_details(request, restaurant_id):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    name = body['name']
    restaurant = Restaurant.objects.get(pk=restaurant_id)
    restaurant.name = name
    restaurant.save()
    return JsonResponse({}, status=200)
