from django.db import models


class Restaurant(models.Model):
    name = models.CharField(max_length=50)
    account = models.ForeignKey('account.Account', on_delete=models.CASCADE)
