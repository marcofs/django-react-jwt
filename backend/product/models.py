from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=50)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    category = models.ForeignKey('category.Category', on_delete=models.CASCADE)
