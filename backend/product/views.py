from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from category.models import Category
from product.models import Product


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_products_by_category(request, restaurant_id, category_id):
    products = list(Product.objects.filter(Q(category_id__exact=category_id)).values())
    return JsonResponse({'products': products}, status=200)
