from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from category import views as category
from restaurant import views as restaurant
from product import views as product

urlpatterns = [
    # API routes
    path('restaurant/<int:restaurant_id>/categories', category.get_categories),
    path('restaurant/<int:restaurant_id>/details', restaurant.update_details),
    path('restaurant/<int:restaurant_id>/categories/<int:category_id>/products', product.get_products_by_category),

    # Admin, token routes
    path('admin/', admin.site.urls),
    path('api/token/access/', TokenRefreshView.as_view(), name='token_get_access'),
    path('api/token/both/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
]
