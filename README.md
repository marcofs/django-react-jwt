# Django + React + JWT

Am example on how JSON Web Tokens work, using Django in the backend and React in the frontend.

## Authentication
![](/docs/session_vs_jwt.png)

There are a few differences between cookie based authentication and JWT based authentication. Session IDs stored in browser cookies need to also be stored in a database, so that the server can validate if the user performing the request has a valid session in the database. With JWT, you don't need to store anything in the database. All the information you need, for example a user ID, can be codified into the token itself.

If you want to dig further, [this is a nice read](https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/) and [this is a good video](https://www.youtube.com/watch?v=7Q17ubqLfaM).


## Business
![](/docs/entities.png)

The business given as an example is simple: there are **accounts** (users) who own **restaurants**. The restaurants have food **categories** and there are **products**, which are food items.

## Running
### Backend
- `python manage.py migrate`
- `python manage.py runserver`

### Frontend
- `npm install`
- `npm start`
