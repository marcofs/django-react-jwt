import { authRequest } from './auth.js'

const getCategories = (restaurantId) => {
  const extraParameters = {params: {}}
  return authRequest.get('/restaurant/1/categories',extraParameters)
    .then(response=>{
      return Promise.resolve(response.data)
    }).catch((error)=> {
      return Promise.reject(error)
    });
}


export { getCategories }
