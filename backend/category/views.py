from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from django.http import JsonResponse

from category.models import Category


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_categories(request, restaurant_id):
    categories = list(Category.objects.filter(Q(restaurant_id__exact=restaurant_id)).values())
    return JsonResponse({'categories': categories}, status=200)
